
const SevenSegmentDisplay = require('./src/devices/seven-segment-display');

const digitChannels = [4, 12, 25, 24];
const segmentChannels = {
  e: 5,
  d: 6,
  dp: 13,
  c: 19,
  g: 26,
  a: 16,
  f: 20,
  b: 21,
};

const display = new SevenSegmentDisplay({
  digitChannels,
  segmentChannels,
});

display.set(8, 3);
display.set(7, 2);
display.set(6, 1);
display.set(5, 0);
