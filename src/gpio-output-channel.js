// ./src/gpio-output-channel.js

const { Gpio } = require('pigpio');

const GpioChannel = require('./gpio-channel');

class GpioOutputChannel extends GpioChannel {
  constructor(channel) {
    super(channel);
    this.channel = new Gpio(channel, {
      mode: Gpio.OUTPUT,
    });
  }

  set(value) {
    this.channel.digitalWrite((value && 1) || 0);
    return this;
  }
}

module.exports = GpioOutputChannel;
