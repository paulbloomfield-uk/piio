// ./src/device.js

class DeviceError extends Error {
  constructor(message, options) {
    const settings = {
      name: 'DeviceError',
      ...options,
    };
    super(message);
    this.name = settings.name;
  }
}

module.exports = DeviceError;
