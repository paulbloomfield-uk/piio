// ./src/on-off-device.js

/* eslint no-bitwise: 0 */

const Device = require('./device');
const DeviceError = require('./device-error');
const BankedGpioChannel = require('../banked-gpio-channel');

class SevenSegmentDisplay extends Device {
  constructor(options) {
    super({
      digitSegments: {
        0: 'abcdef',
        1: 'bc',
        2: 'abged',
        3: 'abgcd',
        4: 'fgbc',
        5: 'afgcd',
        6: 'afedcg',
        7: 'abc',
        8: 'abcdefg',
        9: 'abcdfg',
      },
      ...options,
    });
    this.setMasks();
  }

  setMasks() {
    // Get the masks for the segments to display each value.
    this.segmentMasks = {};
    Object.entries(this.settings.digitSegments).forEach(([digit, segments]) => {
      this.segmentMasks[digit] = segments.split('').reduce(
        (mask, segment) => mask | (1 << this.settings.segmentChannels[segment]),
        0,
      );
    });

    // Get the masks for the channels to display each digit.
    this.digitMasks = this.settings.digitChannels.map((channel) => 1 << channel);

    // Get a mask for all the segments.
    this.segmentMask = Object.values(this.settings.segmentChannels).reduce(
      (mask, channel) => mask | (1 << channel),
      0,
    );

    // Get a mask for all the digits.
    this.digitMask = this.settings.digitChannels.reduce(
      (mask, channel) => mask | (1 << channel),
      0,
    );

    this.channel = new BankedGpioChannel(this.digitMask | this.segmentMask);
  }

  set(value, digit) {
    // Digits are active high, segments are active low.
    this.channel.set(this.digitMasks[digit] | (this.segmentMasks[value] ^ this.segmentMask));
  }
}

module.exports = SevenSegmentDisplay;
