// ./src/on-off-device.js

const Device = require('./device');
const DeviceError = require('./device-error');
const GpioOutputChannel = require('../gpio-output-channel');

class OnOffDevice extends Device {
  constructor(channel) {
    const options = typeof channel === 'number' ? { channel } : channel;
    super({
      inverted: false,
      ...options,
    });
    this.channel = new GpioOutputChannel(this.settings.channel);
    this.invert(this.settings.inverted);
  }

  invert(setting) {
    switch (setting) {
      case true:
        this.settings.inverted = true;
        this.onValue = 0;
        this.offValue = 1;
        return this;

      case false:
        this.settings.inverted = false;
        this.onValue = 1;
        this.offValue = 0;
        return this;

      default:
        this.settings.inverted = !this.settings.inverted;
        this.onValue = 1 - this.onValue;
        this.offValue = 1 - this.offValue;
        return this;
    }
  }

  set(value) {
    switch (value) {
      case OnOffDevice.ON:
        return this.setOn();

      case OnOffDevice.OFF:
        return this.setOff();

      default:
        throw new DeviceError('Invalid device state');
    }
  }

  setOn() {
    this.channel.set(this.onValue);
    return this;
  }

  setOff() {
    this.channel.set(this.offValue);
    return this;
  }
}

OnOffDevice.ON = 1;
OnOffDevice.OFF = 0;

module.exports = OnOffDevice;
