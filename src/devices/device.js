// ./src/device.js

class Device {
  constructor(options) {
    this.settings = {
      ...options,
    };
  }
}

module.exports = Device;
