// ./src/banked-gpio-channel.js

/* eslint no-bitwise: 0, no-plusplus: 0 */

const { Gpio, GpioBank } = require('pigpio');

class BankedGpioChannel {
  constructor(mask) {
    const options = typeof mask === 'number' ? { mask } : mask;
    this.settings = {
      mask: 0,
      ...options,
    };
    this.bank = new GpioBank();
    this.mask = this.settings.mask;

    // Set all the output channels so the bank set and clear operations work.
    const mode = { mode: Gpio.OUTPUT };
    let bitValue = 1;
    for (let bit = 0; bit < 32; bit++) {
      if (this.mask & bitValue) {
        /* eslint-disable-next-line no-new */
        new Gpio(bit, mode);
      }
      bitValue *= 2;
    }
  }

  set(value) {
    this.bank.clear(this.mask & ~value);
    this.bank.set(this.mask & value);
    return this;
  }
}

module.exports = BankedGpioChannel;
